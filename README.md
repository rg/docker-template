# Docker template

This is a skeleton / example / template for a container build, with some tricks and best practises

To get a smaller image :

- Don't install suggestion and recommends (apt based OS only)
- Don't include doc (dpkg based OS only)
- Get rid of the intermediate layers through a multistage build : a RUN rm -rf <dir_to_be_cleaned> don't clean up intermediate layers.

For "application" containers, to avoid writing files in the volumes that'll end up with a weird user on the server running the container :

- Install `apt install -y gosu`
- Set LOCAL_USER_ID & LOCAL_GROUP_ID environment variables
- Use an [entrypoint.sh](./entrypoint.sh) that will start by creating an app user with those UID & GID, run whatever commands you need then step down from root to app user via `gosu`

To make sure a third party application like Postgres is up and ready, use something like wait-for-it before running the application.

For CI/CD containers that need to test systemd services :

- Mount cgroup volume
- Start with systemd
