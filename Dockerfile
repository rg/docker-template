# Use -slim variant if available
FROM debian:10-slim AS build

# Avoid unnecessary files when installing packages
COPY files/dpkg-nodoc /etc/dpkg/dpkg.cfg.d/01_nodoc
COPY files/apt-no-recommends /etc/apt/apt.conf.d/99synaptic

# Build instructions
COPY ./*.sh /usr/local/bin/
COPY ./config.yaml /etc/app/
RUN set -eux && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y gosu wait-for-it && \
    gosu nobody true && \
    # whatever && \
    chmod +x /usr/local/bin/*

# Cleaning up. No need to stack it up too much, since this is a mu#ltistage build
RUN set -eux && \
    apt-get clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/log/* /var/tmp/*



FROM scratch
COPY --from=build / /

ENV LOCAL_USER_ID=12345 \
    LOCAL_GROUP_ID=12345

WORKDIR /var/lib/app
EXPOSE 80

ENTRYPOINT ["docker-entrypoint-production.sh"]
CMD ["stack","serve"]

# For CI container which need to test systemd service
# VOLUME [ "/sys/fs/cgroup" ]
# CMD ["/lib/systemd/systemd"]
