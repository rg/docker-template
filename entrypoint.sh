#!/bin/sh
# set -x

echo "=========================================================================="
echo "Starting $0, $(date)"
echo "=========================================================================="

# Prepare stepping down from root to applicative user with chosen UID/GID
USER_ID=${LOCAL_USER_ID:-12345}
GROUP_ID=${LOCAL_GROUP_ID:-12345}
groupadd -g $GROUP_ID -o app
useradd --shell /bin/bash -u $USER_ID -g app -o -c "app user" -d /var/lib/app -m app
chown -R app: /var/lib/app

# Ensuring THIRDPARTY is ready if running an applicative subcommand
if echo "$@" | grep -q "app serve "; then
    echo "Wait for THIRDPARTY to be available..."
    wait-for-it.sh -h $THIRDPARTY_HOST -p $THIRDPARTY_PORT -t 60

    # Init THIRDPARTY maybe

    # And also wait for Redis if defined
    if [ ! -z "$OTHERTHIRDPARTY" ]; then
        echo "Wait for OTHERTHIRDPARTY to be available..."
        wait-for-it.sh $OTHERTHIRDPARTY -t 60
    fi

    # Then run the command itself as applicative user
    echo "Now running CMD with UID $USER_ID and GID $GROUP_ID"
    exec gosu app "$@"

else

    # Otherwise run the command as root
    exec "$@"
fi
